%%%-------------------------------------------------------------------
%%% @author alex
%%% @copyright (C) 2013, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 11. Nov 2013 10:26 PM
%%%-------------------------------------------------------------------
-module(helpers).
-author("Alexey Shilenkov").

%% API
-export([any_to_list/1, any_to_binary/1, tupleToStringList/2, deleteLast/1, any_to_integer/1]).

-spec(any_to_list/1 ::
    (atom() | integer() | string() | binary()) -> string()).

any_to_list(Atom) when is_atom(Atom) ->
  atom_to_list(Atom);
any_to_list(Bitstring) when is_bitstring(Bitstring) ->
  bitstring_to_list(Bitstring);
any_to_list(Integer) when is_integer(Integer) ->
  integer_to_list(Integer);
any_to_list(String) when is_list(String) ->
  String;
any_to_list(Binary) when is_binary(Binary) ->
  binary_to_list(Binary);
any_to_list(Var)->Var.

-spec(any_to_binary/1 ::
    (atom() | integer() | string() | binary()) -> binary()).

any_to_binary(Atom) when is_atom(Atom) ->
  any_to_binary(atom_to_list(Atom));
any_to_binary(Integer) when is_integer(Integer) ->
  any_to_binary(integer_to_list(Integer));
any_to_binary(String) when is_list(String) ->
  list_to_binary(String);
any_to_binary(Binary) when is_binary(Binary) ->
  Binary;
any_to_binary(Var)->Var.

any_to_integer(Atom) when is_atom(Atom) ->
  any_to_integer(any_to_list(Atom));
any_to_integer(Integer) when is_integer(Integer) ->
  Integer;
any_to_integer(String) when is_list(String) ->
  list_to_integer(String);
any_to_integer(Binary) when is_binary(Binary) ->
  erlang:binary_to_integer(Binary);
any_to_integer(Bitstring) when is_bitstring(Bitstring)->
 any_to_integer(erlang:bitstr_to_list(Bitstring));
any_to_integer(Var)->Var.

tupleToStringList(Tuple, Separator) ->
  [erlang:integer_to_list(Y) || Y <- lists:append([tuple_to_list(X) || X <- Tuple])].

deleteLast(List) ->
  [_ | T] = lists:reverse(List),
  Res = lists:reverse(T).


