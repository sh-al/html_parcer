%%%-------------------------------------------------------------------
%%% @author alex
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 28. Mar 2015 8:27 AM
%%%-------------------------------------------------------------------
-module(parser_depricated).
-author("alex").

-behaviour(gen_server).

%% API
-export([start_link/0]).
-include("../parser.hrl").

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).
-define(Autochel, "http://autochel.ru/car/").


-record(state, {}).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @end
%%--------------------------------------------------------------------
-spec(start_link() ->
  {ok, Pid :: pid()} | ignore | {error, Reason :: term()}).
start_link() ->
  gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
-spec(init(Args :: term()) ->
  {ok, State :: #state{}} | {ok, State :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term()} | ignore).
init([]) ->
  lager:info("Parcer supervisor started"),
  parce(?Autochel),
  {ok, #state{}}.


parce(Link) ->
  {ok, {Status, Headers, Body}} = httpc:request(Link),
  Tree = mochiweb_html:parse(Body),
  parce_t7vendors(mochiweb_xpath:execute("//li[@class='t7']//a[@href]", Tree)).


parce_t7vendors([H | T]) ->
  {_A, [{_Href, Href}], [Name]} = H,
  case link_to_models(Href) of
    [Link] ->
      %Result = lists:append([Acc, [Link, Name]]),
      parce_models(?toList(Link));
    _ -> lager:debug("Error, cannot match vendor", [])
  end.
%%   ,  parce_t7vendors(T);            %%TODO parce only single vendor
%% parce_t7vendors([]) -> lager:debug("finished").

%%PARCING MODELS, INPUT=link from vendor

parce_models(Link) ->
  {ok, {Status, Headers, Body}} = httpc:request(Link),
  Tree = mochiweb_html:parse(Body),
  Models = parce_t7models(mochiweb_xpath:execute("//li[@class='t7']//a[@href]", Tree)),
  lager:debug("parce_models link ~p~n ", [Models]).% parce_t7models(mochiweb_xpath:execute("//li[@class='t7']//a[@href]", Tree), []);


parce_t7models([H | T]) ->
  %lager:debug("H ~p~n", [H]),
  case H of
  %{_A, [{_, _}, {_Href, Href}], [Model]} -> lager:debug("Href ~p Model ~p~n", [Href, Model]), parce_t7models(T);
    {_A, [{_Href, Href}], [Model]} -> lager:debug("2: Href ~p Model ~p~n", [Href, Model])
      , parce_cars_list(Href);
  %, parce_t7models(T);
    _ -> lager:debug("Cannot match model", [])
      , parce_t7models(T)
  end;
parce_t7models([]) -> lager:debug("finished parsing ~n~n").


link_to_models(Link) ->
  {ok, MP} = re:compile("motors"), %Motors- parce only cars
  case re:run(Link, MP, [{capture, all_but_first, list}]) of
    {match, _} -> [Link];
    _ -> []
  end.


%%PARCE CAR LIST
%00:40:01.737 [debug] 2: Href <<"/car/motors/rus/lada/1111/">> Model <<"1111">>

parce_cars_list(LLink) ->
  Link = ?toList(?toBinary([<<"http://autochel.ru">>, LLink])),
  lager:debug("Link to cars list ~p~n", [Link]),
  {ok, {Status, Headers, Body}} =httpc:request(Link),
  Tree = mochiweb_html:parse(Body),
%%   lager:debug("Treee ~p~n", [Tree]),
  % Css = remove_duplicates(mochiweb_xpath:execute("//link[@rel=’stylesheet’]/@href",Tree)),
  Cars = parce_cars(mochiweb_xpath:execute("//div[@class='photo_container']//a[@href]", Tree)),
  lager:debug("Got  Cars ~p~n", [Cars]),
  ok.

parce_cars([H | T]) ->
  case H of
    {_A, [_,_, {_Href, Href}],_} -> lager:debug("Got Car Href ~p~n", [Href]),
      parce_car(Href),
      parce_cars(T);
    _-> lager:debug("can not match") , parce_cars(T)
  end;
parce_cars([])-> ok.



parce_car(LLink)->
  Link = ?toList(?toBinary([<<"http://autochel.ru">>, LLink])),
  {ok, {Status, Headers, Body}} =httpc:request(Link),
  Tree = mochiweb_html:parse(Body),
  [{ _Div, _DetailsField, Car}] = mochiweb_xpath:execute("//div[@class='details_fields']", Tree),
  CarsParams=parce_car_values(Car, []),
  lager:debug("Car params ~p~n", [CarsParams]),
  ok.

parce_car_values([H|T], Acc)->
  case H of
    {_Div, _, [_Span, Value] }-> Result = [Acc, Value] ;
    _->Result=Acc
  end,
  parce_car_values(T,Result);
parce_car_values([], Result)-> lists:flatten(Result).

%%   case H of
%%   %{_A, [{_, _}, {_Href, Href}], [Model]} -> lager:debug("Href ~p Model ~p~n", [Href, Model]), parce_t7models(T);
%%     {_A, [{_Href, Href}], [Model]} -> lager:debug("2: Href ~p Model ~p~n", [Href, Model])
%%       , parce_cars_list(Href);
%%   %, parce_t7models(T);
%%     _ -> lager:debug("Cannot match model", [])
%%       , parce_cars(T)
%%   end;


%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
    State :: #state{}) ->
  {reply, Reply :: term(), NewState :: #state{}} |
  {reply, Reply :: term(), NewState :: #state{}, timeout() | hibernate} |
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), Reply :: term(), NewState :: #state{}} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_call(_Request, _From, State) ->
  {reply, ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_cast(Request :: term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_cast(_Request, State) ->
  {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
-spec(handle_info(Info :: timeout() | term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_info(_Info, State) ->
  {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
    State :: #state{}) -> term()).
terminate(_Reason, _State) ->
  ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
-spec(code_change(OldVsn :: term() | {down, term()}, State :: #state{},
    Extra :: term()) ->
  {ok, NewState :: #state{}} | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================


%% page_info(URL) ->
%%   case http:request(URL) of
%%     {ok,{_,Headers,Body}} ->
%%       got_page_info(URL,content_length(Headers),Body);
%%     {error,Reason} ->
%%       {error,Reason}
%%   end.

