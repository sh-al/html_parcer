-module(parser_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(CHILD(I, Type), {I, {I, start_link, []}, permanent, 5000, Type, [I]}).

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
  %%init modules
  inets:start(),

%%   application:start(syntax_tools),
%%   application:start(compiler),
%%   application:start(goldrush),
  application:start(lager),

  %init lager
  lager:start(),
  lager:set_loglevel(lager_console_backend, debug),
  lager:debug("START parser"),

  supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([]) ->
  {ok, {{one_for_one, 5, 10}, [?CHILD(parser, supervisor)]}}.

