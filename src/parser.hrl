%%%-------------------------------------------------------------------
%%% @author alex
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 28. Mar 2015 9:52 AM
%%%-------------------------------------------------------------------
-author("alex").

-define(toList(Term), helpers:any_to_list(Term)).
-define(toBinary(Term), helpers:any_to_binary(Term)).
-define(toInt(Term), helpers:any_to_integer(Term)).
-define(delLast(Term), helpers:deleteLast(Term)).

