%%%-------------------------------------------------------------------
%%% @author alex
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 31. Mar 2015 5:37 AM
%%%-------------------------------------------------------------------
-module(parser).
-author("alex").

-behaviour(gen_server).

-include("../parser.hrl").
%% API
-export([start_link/0]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-define(Autochel, "http://autochel.ru/car/").

-define(CLASS_LI_T7, "//li[@class='t7']//a[@href]").
-define(CLASS_DIV_PHOTO_CONTAINER, "//div[@class='photo_container']//a[@href]").
-define(CLASS_DIV_DETAILS_FIELD, "//div[@class='details_fields']").

-record(state, {}).

%%%===================================================================
%%% API
%%%===================================================================

-spec(start_link() ->
  {ok, Pid :: pid()} | ignore | {error, Reason :: term()}).
start_link() ->
  gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================
is_car(String, Arg) -> string:str(String, Arg) > 0.
-spec(init(Args :: term()) ->
  {ok, State :: #state{}} | {ok, State :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term()} | ignore).
init([]) ->

  Vendors = parce_by_tag(?Autochel, ?CLASS_LI_T7),
%  IsCar = fun(String, Arg)-> string:str(String,Arg) end ,
  [Vendor | _] = lists:map(fun({_A, [{_Href, Href}], [Name]} = _I) -> [Href, Name] end, Vendors),
  lager:debug("Vendor ~p~n", [Vendor]),
  [VendorModel | _] = Vendor,
  ParcedModels = parce_by_tag(?toList(VendorModel), ?CLASS_LI_T7),
  %({<<"a">>,[{<<"class">>,<<"attention">>},{<<"href">>,<<"/car/motors/rus/lada/all/">>}]
  ModelsList = lists:map(fun(I) ->
    case I of
      {_A, [{_Href, Href}], [Model]} -> [Href, Model];
      _ -> []
    end
  end, ParcedModels),
  lager:debug("Vendor ~p~n Models ~p~n", [Vendor, ModelsList]),
  {ok, #state{}}.
%%--------------------------------------------------------------------
-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
    State :: #state{}) ->
  {reply, Reply :: term(), NewState :: #state{}} |
  {reply, Reply :: term(), NewState :: #state{}, timeout() | hibernate} |
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), Reply :: term(), NewState :: #state{}} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_call(_Request, _From, State) ->
  {reply, ok, State}.
%%--------------------------------------------------------------------
-spec(handle_cast(Request :: term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_cast(_Request, State) ->
  {noreply, State}.

%%--------------------------------------------------------------------
-spec(handle_info(Info :: timeout() | term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_info(_Info, State) ->
  {noreply, State}.

%%--------------------------------------------------------------------
-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
    State :: #state{}) -> term()).
terminate(_Reason, _State) ->
  ok.

%%--------------------------------------------------------------------
-spec(code_change(OldVsn :: term() | {down, term()}, State :: #state{},
    Extra :: term()) ->
  {ok, NewState :: #state{}} | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->

  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

%%check is there is no http://site.com/ in link
format_link(Link, Arg) ->
  {ok, MP} = re:compile(Arg), %Motors- parce only cars
  case re:run(Link, MP, [{capture, all_but_first, list}]) of
    {match, _} -> ?toList(Link);
    _ -> ?toList(?toBinary([Link, Arg]))
  end.

parce_by_tag(Link, Tag) ->
  case httpc:request(Link) of
    {ok, {{_, RetCode, _}, _Headers, Body}} ->
      if RetCode == 200;RetCode == 201 ->
        Tree = mochiweb_html:parse(Body),
        mochiweb_xpath:execute(Tag, Tree);
        RetCode >= 500 ->
          lager:error("returned 500"), {};
        true -> lager:error("Error retCode", []), {}
      end;
    {error, {failed_connect, [{_, {Addr, Port}}, _]}} ->
      lager:error("Can not connect to ~p on port ~p~n", [Addr, Port]);
    Err -> lager:error("Can not pace Link ~p by TAG ~p~n, ~p~N", [Link, Tag, Err]), {}
  end.


%IsCam = fun(X) -> string:str(X, "cam") > 0 end,

filter_link(Link, Filter) ->
  case string:str(Link, Filter) of
    _ -> [Link];
    0 -> []
  end.

filter_link_re(Link, Filter) ->
  {ok, MP} = re:compile(Filter), %Motors- parce only cars
  case re:run(Link, MP, [{capture, all_but_first, list}]) of
    {match, _} -> [Link];
    _ -> []
  end.


%error(String, Args) -> lager:error(String, Args).